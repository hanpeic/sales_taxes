Ruby Tech Test
=======================

"Sales Taxes"

# Usage

1 Install ruby

2 Download the code: git clone https://tom_han@bitbucket.org/tom_han/sales_taxes.git

3 Run command: bundle install

4 Start the server: rails s

5 Open http://localhost:3000 in browser

6 Enjoy it!

# Test
Run command: rspec